
$(function () {

    // tabs

    $(".tab-title").click(function() {
        const selectedTab = this
        $.each($(".tab-title"), function() {
            this.classList.remove("active")
            
        })
        selectedTab.classList.add("active")
        console.log(selectedTab);
        $.each($(".tab-content"), function() {
            $.style(this, "display", "none")
            if (selectedTab.dataset.name === this.dataset.name) {
                $.style(this, "display", "flex")
            }
        })
    })

    // filter

    $(".filter-tab-item").click(function () {
        if ($(this).data("name") !== "all") {
            $(".filter-image-item").hide().filter("." + $(this).data("name")).slideToggle(600)
        } else {
            $(".filter-image-item").toggle()
        }
    })

    $('.img-load-button').click(function () {
        $('.filter-images li:hidden').slice(0, 12).slideToggle();
        if ($('.filter-images li').length === $('.filter-images li:visible').length) {
            $('.img-load-button').hide();
        }
    })

    // slider

    $('.slider-navigation').slick( {
        asNavFor: ".slider-content",
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev"> < </button>',
        nextArrow: '<button type="button" class="slick-next"> > </button>',
        arrows: true,
        focusOnSelect: true,
    });
    $('.slider-content').slick( {
        asNavFor: '.slider-navigation',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    });

})